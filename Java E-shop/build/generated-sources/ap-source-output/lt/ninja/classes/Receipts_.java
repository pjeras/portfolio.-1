package lt.ninja.classes;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Receipts.class)
public abstract class Receipts_ {

	public static volatile ListAttribute<Receipts, Receipsrows> receipsrowsList;
	public static volatile SingularAttribute<Receipts, Integer> id;
	public static volatile SingularAttribute<Receipts, Date> receipDate;
	public static volatile SingularAttribute<Receipts, String> info;

	public static final String RECEIPSROWS_LIST = "receipsrowsList";
	public static final String ID = "id";
	public static final String RECEIP_DATE = "receipDate";
	public static final String INFO = "info";

}

