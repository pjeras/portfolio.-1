package lt.ninja.classes;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Cartrows.class)
public abstract class Cartrows_ {

	public static volatile SingularAttribute<Cartrows, Integer> quantity;
	public static volatile SingularAttribute<Cartrows, BigDecimal> price;
	public static volatile SingularAttribute<Cartrows, Cart> cartId;
	public static volatile SingularAttribute<Cartrows, Integer> id;
	public static volatile SingularAttribute<Cartrows, Goods> goodId;

	public static final String QUANTITY = "quantity";
	public static final String PRICE = "price";
	public static final String CART_ID = "cartId";
	public static final String ID = "id";
	public static final String GOOD_ID = "goodId";

}

