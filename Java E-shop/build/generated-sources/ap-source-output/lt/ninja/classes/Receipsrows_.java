package lt.ninja.classes;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Receipsrows.class)
public abstract class Receipsrows_ {

	public static volatile SingularAttribute<Receipsrows, Integer> quantity;
	public static volatile SingularAttribute<Receipsrows, Integer> id;
	public static volatile SingularAttribute<Receipsrows, Goods> goodId;
	public static volatile SingularAttribute<Receipsrows, Receipts> receipId;

	public static final String QUANTITY = "quantity";
	public static final String ID = "id";
	public static final String GOOD_ID = "goodId";
	public static final String RECEIP_ID = "receipId";

}

