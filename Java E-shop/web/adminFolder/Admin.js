let counter = 0;

function addNewReceiveline() {
    request = $.ajax({method: "GET", url: "/NinjaCode/rest/ninjatools/getGoods"});
    request.done(response => {
        let newOption = "<tr><td><select id=\"select" + counter + "\">";
        for (let i = 0; i < response.length; i++) {
            newOption += "<option value=\"" + response[i].name + "\" id=\"" + response[i].id + "\">" + response[i].name + "</option>";
        }
        newOption += "</select></td><td><input type=\"number\" placeholder=\"Quantity\" value=1 id=\"inputQuant" + counter + "\" min=\"1\"></td></tr>";
        $("#receivings").append(newOption);
        counter++;
    });
}
;
$("#addNewReceiveItem").click(function () {
    addNewReceiveline();
});

$("#addNewReceiving").click(function () {


    let receivingDate = new Date($("#receivingDate").val());

    let receivingInfo = $("#receivingInfo").val();

    var array = new Array();

    for (let i = 0; i < counter; i++) {
        let goodId = $("select#select" + i + " option:selected").attr("id");
        let quantity = $("#inputQuant" + i).val();


        let receivinglineObj = {
            goodId: goodId,
            quantity: quantity,
            receivingDate: receivingDate,
            receivingInfo: receivingInfo
        };
        alert(goodId + "hhh" + quantity + "hhh" + receivingDate + "hhh" + receivingInfo);
        array.push(receivinglineObj);
    }


    if (receivingDate === null || receivingDate === undefined || receivingDate === "") {
        alert("Please check inputs");
    } else {
        request = $.ajax({
            method: "POST", url: "/NinjaCode/rest/ninjatools/addNewReceiving", data: JSON.stringify(array), contentType: "application/json; charset=utf-8", dataType: "json"});
        request.done(function () {
            window.location.reload();
        });
    }
});

$("#addgoodbutton").click(function () {

    let name = $("#name").val();
    let quantity = $("#quantity").val();
    let price = $("#price").val();
    let description = $("#description").val();


    let good = {
        name: name,
        quantity: quantity,
        price: price,
        description: description

    };

    if (name !== null && name !== undefined && name !== "" && quantity !== null && quantity !== undefined && quantity !== "" && price !== null && price !== undefined && price !== "" && description !== null && description !== undefined && description !== "") {
        request = $.ajax({url: "/NinjaCode/rest/ninjatools/addgood", method: "POST", data: JSON.stringify(good), contentType: "application/json; charset=utf-8", dataType: "json"});
        request.done(response => {
            window.location.reload();
        });
    }
});

