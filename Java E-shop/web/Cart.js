$(document).ready(function () {
    request = $.ajax({url: "rest/ninjatools/getUserName", dataType: "text", method: "GET"});
    request.done(response => {
        document.getElementById("name").innerHTML = response;
        if (response != "") {
            $("#logout").show();
            $("#login").hide();
        } else {
            $("#logout").hide();
            $("#login").show();
        }
    });
    request.fail(response => {

        $("#logout").hide();
        $("#login").show();
    });
});


$(document).ready(function () {
    request = $.ajax({url: "rest/ninjatools/getAdmin", dataType: "text", method: "GET"});
    request.done(response => {
        if (response == 1) {
            $("#admin").show();
        } else {
            $("#admin").hide();
        }

    });
    request.fail(response => {

        $("#admin").hide();

    });
});



$("#logout").click(function () {
    request = $.ajax({url: "rest/ninjatools/killsession", method: "POST", contentType: "application/json; charset=utf-8", dataType: "json"});
    request.done(response => {
        $("#name").hide();
        $("#logout").hide();
        $("#admin").hide();
        $("#login").show();
        window.location.href = "/NinjaCode";
        document.getElementById("quant").innerHTML = 0;

    });
});

$(document).ready(function () {
    request = $.ajax({url: "rest/ninjatools/getCartProducts", method: "GET"});
    request.done(response => {
        $("#carttbody").empty();
        console.log(response);
        let total = 0;
        for (let i = 0; i < response.length; i++) {
            let newTr = "<tr>";
            newTr += "<td>" + response[i].productName + "</td>";
            newTr += "<td>" + "<input class='checkquantity' type='number' max='999' min='1' value='" + response[i].productQuantity + "' data-cartRowId='" + response[i].productRowId + "'>" + "</td>";
            newTr += "<td>" + response[i].productPrice + '€' + "</td>";
            newTr += "<td>" + "<button class='deleteRow' value='" + response[i].productRowId + "'>" + "Remove" + "</button>" + "</td>";
            newTr += "</tr>";

            $("#carttbody").append(newTr);
            total += response[i].productPrice;


        }
        $(".deleteRow").click(function (e) {

            let rowId = e.currentTarget.value;

            request = $.ajax({url: "rest/ninjatools/deleteRow?rowId=" + rowId, method: "DELETE"});
            request.done(response => {

                location.reload();
            });
            request.fail(response => {
            });
        });
        document.getElementById("total").innerHTML = total.toFixed(2) + "€";
        
        
        $(".buybutton").click(function () {
            request = $.ajax({url: "rest/ninjatools/buy", method: "POST"});
            request.done(response => {

                if (response === 1) {
                    window.location.href = "Login.html";
                } else {
                    window.location.href = "Main.html";
                }
            });

        });
        
        $(".checkquantity").change(function () {
            let quantity = $(this).val();
            let id = $(this).attr("data-cartRowId");


            let cartRowUpdate = {
                quantity: quantity,
                id: id
            };

            request = $.ajax({url: "rest/ninjatools/updateCartProducts", method: "POST", data: JSON.stringify(cartRowUpdate), contentType: "application/json; charset=utf-8", dataType: "text"});
            request.done(response => {
                if (response !== "") {
                    alert(response);
                }
                location.reload();
            });
        });
    });
});

$(".continue").click(function () {
    history.back();
});



