
$(".registerbutonas").click(function () {

    let userName = $("#userName").val();
    let password = $("#password").val();
    let password2 = $("#password2").val();

    let usern = {
        userName: userName
    };
    if (userName === null || userName === undefined || userName === "") {
        document.getElementById("pranesimas").innerHTML = "Please enter username!";

    } else if (password === null || password === undefined || password === "") {
        document.getElementById("pranesimas").innerHTML = "Please enter password!";

    } else if (password2 === null || password2 === undefined || password2 === "") {
        document.getElementById("pranesimas").innerHTML = "Please repeat password!";

    } else if (password !== password2) {
        document.getElementById("pranesimas").innerHTML = "Passwords should match!";


    } else {
        if (userName.match(/[A-Za-z0-9]/)) {
            if (userName.length <= 40 && userName.length >= 1) {
                if (password.length >= 6 && password.length <= 40) {
                    if (userName !== null || userName !== undefined || userName !== "") {
                        request = $.ajax({url: "rest/ninjatools/username", method: "POST", data: JSON.stringify(usern), contentType: "application/json; charset=utf-8", dataType: "json"});
                        request.done(response => {
                            if (response === 1) {
                                document.getElementById("pranesimas").innerHTML = "This username already exist!";
                            } else if (response === 0) {


                                let data = {
                                    userName: userName,
                                    password: password
                                
                                };



                                request = $.ajax({url: "rest/ninjatools/register", method: "POST", data: JSON.stringify(data), contentType: "application/json; charset=utf-8", dataType: "json"});
                                request.done(response => {
                                    window.location.href = "Login.html";
                                });
                            }
                        });

                    } else {
                        document.getElementById("pranesimas").innerHTML = "Please enter username!";
                    }
                } else {
                    document.getElementById("pranesimas").innerHTML = "Password should contain between 6 and 40 digits!";
                }
            } else {
                document.getElementById("pranesimas").innerHTML = "Username should contain between 1 and 40 digits!";
            }
        } else {
            document.getElementById("pranesimas").innerHTML = "Username should contain only latin letters and numbers!";
        }

    }
});
