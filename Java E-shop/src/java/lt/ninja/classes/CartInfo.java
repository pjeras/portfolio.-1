/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.ninja.classes;

import java.math.BigDecimal;

/**
 *
 * @author Lukas
 */
public class CartInfo {

    private String productName;
    private int productQuantity;
    private BigDecimal productPrice;
    private Integer productRowId;

    public CartInfo(String productName, int productQuantity, BigDecimal productPrice, Integer productRowId) {
        this.productName = productName;
        this.productQuantity = productQuantity;
        this.productPrice = productPrice;
        this.productRowId = productRowId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    public Integer getProductRowId() {
        return productRowId;
    }

    public void setProductRowId(Integer productRowId) {
        this.productRowId = productRowId;
    }

    @Override
    public String toString() {
        return "CartInfo{" + "productName=" + productName + ", productQuantity=" + productQuantity + ", productPrice=" + productPrice + ", productRowId=" + productRowId + '}';
    }

}
