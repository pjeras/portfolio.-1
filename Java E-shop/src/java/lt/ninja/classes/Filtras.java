/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.ninja.classes;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Lukas
 */
@WebFilter(filterName = "Filtras", urlPatterns = {"/adminFolder/*"})
public class Filtras implements Filter {

    @Override
    public void init(FilterConfig fc) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain fc) {
        Users user = null;
        try {
            HttpSession session = ((HttpServletRequest) request).getSession(false);

            if (session == null || ((Users) session.getAttribute("userFromDb")).getIsAdmin() != 1 || ((Users) session.getAttribute("userFromDb")) == null) {
                ((HttpServletResponse) response).sendRedirect("/NinjaCode/Main.html");
            } else {
                fc.doFilter(request, response);
            }
        } catch (Exception e) {

        }

    }

    @Override
    public void destroy() {

    }

}
