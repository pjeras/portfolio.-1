/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.ninja.classes;

import java.security.MessageDigest;

/**
 *
 * @author Lukas
 */
public class HashGenerator {

    public static String makeHash(String pass, String salt) {
        char[] passChars = pass.toCharArray();
        char[] saltChars = salt.toCharArray();

        StringBuilder buildPass = new StringBuilder();

        for (int i = 0; i < passChars.length; ++i) {
            buildPass.append(passChars[i]);
            if (i < saltChars.length) {
                buildPass.append(saltChars[i]);
            }
        }
        System.out.println(buildPass);

        try {

            MessageDigest digest = MessageDigest.getInstance("SHA-512");
            byte[] hash = digest.digest(pass.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < hash.length; ++i) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) {
                    hexString.append("0");
                }
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);

        }

    }
}
