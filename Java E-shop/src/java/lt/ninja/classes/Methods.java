/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.ninja.classes;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import static lt.ninja.classes.HashGenerator.makeHash;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Lukas
 */
@Path("/ninjatools")
public class Methods {

    @Context
    HttpServletRequest request;
    private static final Log log = LogFactory.getLog(Methods.class);

    @POST
    @Path("/register")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Users register(Users user) throws Exception {

        log.info("we are here: " + user.getUserName());
        EntityManager em = EMF.getEntityManager();
        EntityTransaction tx = EMF.getTransaction(em);
        String salt = Integer.toString(Randomgen.gen());
        short isAdmin = 0;
        String password = makeHash(user.getPassword(), salt);
        if (user.getId() == null) {

            user.setPassword(password);
            user.setSalt(salt);
            user.setIsAdmin(isAdmin);

            em.persist(user);
        }
        EMF.commitTransaction(tx);
        EMF.returnEntityManager(em);

        return user;
    }
    
    //tikrinama ar username egzistuoja registracijos metu
    @POST
    @Path("/username")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public int username(Users user) throws Exception {

        EntityManager em = EMF.getEntityManager();
        EntityTransaction tx = EMF.getTransaction(em);

        int b = 0;
        Object useris = null;
        try {
            Query query = em.createQuery("SELECT u FROM Users u WHERE u.userName = :userName");
            query.setParameter("userName", user.getUserName());
            useris = query.getSingleResult();

        } catch (NoResultException e) {

        }
        if (useris == null) {
            b = 0;
        } else {
            b = 1;
        }
        EMF.commitTransaction(tx);
        EMF.returnEntityManager(em);

        return b;
    }

    //prisijungimas, tikrinama ar atitinka username ir password
    @POST
    @Path("/log")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public int log(Users user) throws Exception {

        EntityManager em = EMF.getEntityManager();
        EntityTransaction tx = EMF.getTransaction(em);

        HttpSession session = null;
        int b = 0;
        Users useris = null;
        try {
            Query query = em.createQuery("SELECT u FROM Users u WHERE u.userName = :userName");
            query.setParameter("userName", user.getUserName());

            useris = (Users) query.getSingleResult();
            String passwordFromDb = useris.getPassword();
            String password = makeHash(user.getPassword(), useris.getSalt());

            if (password.equals(passwordFromDb)) {
                b = 1;
                session = request.getSession(true);
                session.setAttribute("userFromDb", useris);
            } else {
                b = 0;
            }
        } catch (NoResultException e) {
            b = 0;
        }
        //pakeiciamas user id krepselyje is null i egzistuojanti jei vartotojas prisijungia
        if (session.getAttribute("getCart") != null) {
            Cart cartas = (Cart) session.getAttribute("getCart");
            Cart cart = em.find(Cart.class, cartas.getId());
            cart.setUserId(useris);
        }

        EMF.commitTransaction(tx);
        EMF.returnEntityManager(em);

        return b;
    }

    //gaunamas username, kuris atvaizduojamas jei vartotojas prisijunges
    @GET
    @Path("/getUserName")
    @Produces(MediaType.TEXT_PLAIN)
    public String getUserName() throws Exception {

        Users user = null;

        try {
            HttpSession session = request.getSession(false);
            user = (Users) session.getAttribute("userFromDb");
        } catch (Exception e) {
            return "";
        }
        return user.getUserName();
    }

    //tikrinama ar vartotojas turi admin teises
    @GET
    @Path("/getAdmin")
    @Produces(MediaType.TEXT_PLAIN)
    public int getAdmin() {

        Users user = null;
        try {
            HttpSession session = request.getSession(false);
            user = (Users) session.getAttribute("userFromDb");

        } catch (Exception e) {
            return 0;
        }

        return user.getIsAdmin();
    }

    @POST
    @Path("/killsession")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void logout() throws Exception {
        try {
            HttpSession session = request.getSession(false);
            session.invalidate();
        } catch (Exception e) {

        }
    }

    //naujos prekes pridejimas, kuria gali prideti tik adminas
    @POST
    @Path("/addgood")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addgood(Goods good) throws Exception {
        log.info("we are here: " + good.getName());
        EntityManager em = EMF.getEntityManager();
        EntityTransaction tx = EMF.getTransaction(em);
        em.persist(good);
        EMF.commitTransaction(tx);
        EMF.returnEntityManager(em);

    }

    //gaunamas visu prekiu asortimentas, kuris atvaizzduojamas Main puslapyje
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getGoods")
    public List getGoods() {
        EntityManager em = EMF.getEntityManager();
        Query query = em.createQuery("SELECT g FROM Goods g");
        List goodsList = query.getResultList();
        EMF.returnEntityManager(em);
        return goodsList;

    }

    @POST
    @Path("addtocart")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public int cartadd(@QueryParam("goodId") Integer goodId, Cartrows cartrows) throws Exception {

        EntityManager em = EMF.getEntityManager();
        EntityTransaction tx = EMF.getTransaction(em);
        Users user = null;
        Cart cart = null;
        HttpSession session = request.getSession(true);

        user = (Users) session.getAttribute("userFromDb");
        cart = (Cart) session.getAttribute("getCart");

        //tikrinu ar cart egzistuoja
        if (cart == null) {
            cart = new Cart();
            cart.setCreateDate((new Date()));
            //jei cart neegzistuoja tada tikrinu ar yra useris ar anonimas
            if (user != null) {
                cart.setUserId(user);
            }
            em.persist(cart);

            session.setAttribute("getCart", cart);
        }
        Goods good = em.find(Goods.class, goodId);
        
        //tikrinu ar yra krepselyje vienoda preke
        Cartrows cartrow1 = null;
        try {
            Query query = em.createQuery("SELECT c FROM Cartrows c WHERE c.cartId = :cartid AND c.goodId= :goodid");
            query.setParameter("cartid", cart);
            query.setParameter("goodid", good);
            cartrow1 = (Cartrows) query.getSingleResult();
        } catch (Exception e) {
            cartrow1 = null;

        }
        //jei preke jau egzistuoja krepselyje ir vartotojas tokia pacia preke vel deda i krepseli tai tos prekes kiekius bei suma susumuoja
        if (cartrow1 != null) {
            cartrow1.setQuantity(cartrow1.getQuantity() + cartrows.getQuantity());
            BigDecimal extraSum = BigDecimal.valueOf(cartrows.getQuantity()).multiply(good.getPrice());
            BigDecimal totalSum = (cartrow1.getPrice()).add(extraSum);
            cartrow1.setPrice(totalSum);
            em.persist(cartrow1);
        } else {
            cartrows.setCartId(cart);
            cartrows.setGoodId(good);
            cartrows.setPrice((good.getPrice()).multiply(BigDecimal.valueOf(cartrows.getQuantity())));
            em.persist(cartrows);
        }

        EMF.commitTransaction(tx);
        EMF.returnEntityManager(em);
        EMF.returnEntityManager(em);

        em = EMF.getEntityManager();

        //skaiciuojama kiek skirtingu prekiu rusiu vartotojas deda i savo krepseli
        Cart cartQuantity = null;
        List<Cartrows> rowsList;
        int quant = 0;
        try {
            session = request.getSession(false);
            cartQuantity = (Cart) session.getAttribute("getCart");
            Query query = em.createQuery("SELECT c FROM Cartrows c WHERE c.cartId = :cartid");
            query.setParameter("cartid", cartQuantity);
            rowsList = query.getResultList();
            for (int i = 0; i < rowsList.size(); i++) {
                quant += 1;
            }

        } catch (Exception e) {
            return 0;
        } finally {
            EMF.returnEntityManager(em);
        }

        return quant;

    }

    //tikrinu kiek skirtingu rusiu prekiu vartotojas turi savo krepselyje
    @GET
    @Path("/getProductsQuantity")
    @Produces(MediaType.TEXT_PLAIN)
    public int getQuantity() {
        EntityManager em = EMF.getEntityManager();

        Cart cart = null;
        List<Cartrows> rowsList;
        int quant = 0;
        try {
            HttpSession session = request.getSession(false);
            cart = (Cart) session.getAttribute("getCart");
            Query query = em.createQuery("SELECT c FROM Cartrows c WHERE c.cartId = :cartid");
            query.setParameter("cartid", cart);
            rowsList = query.getResultList();
            for (int i = 0; i < rowsList.size(); i++) {
                quant += 1;
            }

        } catch (Exception e) {
            return 0;
        } finally {
            EMF.returnEntityManager(em);
        }

        return quant;

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getCartProducts")
    public List getCartProducts() {
        EntityManager em = EMF.getEntityManager();
        List<CartInfo> cartGoods = new ArrayList<>();
        Cart cart = null;
        try {
            HttpSession session = request.getSession(false);
            cart = (Cart) session.getAttribute("getCart");
            Query query1 = em.createQuery("SELECT g FROM Goods g");
            List<Goods> goodsList = query1.getResultList();
            Query query2 = em.createQuery("SELECT c FROM Cartrows c WHERE c.cartId = :cartid");
            query2.setParameter("cartid", cart);
            List<Cartrows> cartRowsList = query2.getResultList();
            for (int i = 0; i < cartRowsList.size(); i++) {
                String goodName = null;
                Integer goodId = cartRowsList.get(i).getGoodId().getId();
                for (int j = 0; j < goodsList.size(); j++) {
                    if (goodId == goodsList.get(j).getId()) {
                        goodName = goodsList.get(j).getName();
                    }
                }
                cartGoods.add(new CartInfo(goodName, cartRowsList.get(i).getQuantity(), cartRowsList.get(i).getPrice(), cartRowsList.get(i).getId()));

            }
        } catch (Exception e) {
            return null;
        }
        EMF.returnEntityManager(em);
        return cartGoods;

    }

    @DELETE
    @Path("deleteRow")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteRow(@QueryParam("rowId") Integer rowId) throws Exception {

        EntityManager em = EMF.getEntityManager();

        Cartrows row = em.find(Cartrows.class, rowId);
        EntityTransaction tx = EMF.getTransaction(em);
        em.remove(row);
        EMF.commitTransaction(tx);
        EMF.returnEntityManager(em);

    }

    @POST
    @Path("/addNewReceiving")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addNewReceiving(ReceivingWrapper[] receivingArray) throws Exception {

        EntityManager em = EMF.getEntityManager();
        try {
            EntityTransaction tx = EMF.getTransaction(em);
            Receipts receipt = new Receipts();
            receipt.setReceipDate(receivingArray[0].getReceivingDate());
            receipt.setInfo(receivingArray[0].getReceivingInfo());
            //log.info("RECEIVING: " + receivingArray[0].toString());
            em.persist(receipt);
            // log.info("addingNewReceving" + receipt.toString());
            em.flush();
            for (int i = 0; i < receivingArray.length; i++) {
                Receipsrows receiptrow = new Receipsrows();
                Query query = em.createQuery("SELECT g FROM Goods g WHERE g.id = :id");
                query.setParameter("id", receivingArray[i].getGoodId());
                Goods good = (Goods) query.getSingleResult();

                receiptrow.setGoodId(good);
                receiptrow.setQuantity(receivingArray[i].getQuantity());
                receiptrow.setReceipId(receipt);
                good.setQuantity(good.getQuantity() + receivingArray[i].getQuantity());

                em.persist(receiptrow);
            }
            EMF.commitTransaction(tx);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            EMF.returnEntityManager(em);
        }
    }

    @POST
    @Path("/updateCartProducts")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public String changeQuantity(Cartrows row) {
        EntityManager em = EMF.getEntityManager();
        String message = "";
        int quant = row.getQuantity();
        try {
            EntityTransaction tx = EMF.getTransaction(em);
            Query query = em.createNamedQuery("Cartrows.findById");
            query.setParameter("id", row.getId());
            Cartrows cartrow = (Cartrows) query.getSingleResult();
            if (quant < 1 || quant > cartrow.getGoodId().getQuantity()) {
                quant = cartrow.getGoodId().getQuantity();
                message = "Wrong quantity!";
            }
            cartrow.setQuantity(quant);
            cartrow.setPrice((cartrow.getGoodId().getPrice()).multiply(BigDecimal.valueOf(cartrow.getQuantity())));

            EMF.commitTransaction(tx);
        } catch (Exception e) {
            return "";
        } finally {
            EMF.returnEntityManager(em);
        }

        return message;

    }

    @POST
    @Path("buy")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public int buy() {
        EntityManager em = EMF.getEntityManager();
        int checkIfLogged = 0;
        try {

            EntityTransaction tx = EMF.getTransaction(em);
            HttpSession session = request.getSession(false);
            if (session.getAttribute("userFromDb") == null) {
                checkIfLogged = 1;
            } else {
                Cart cartFind = (Cart) session.getAttribute("getCart");
                 Query queryCart = em.createNamedQuery("Cart.findById");
                queryCart.setParameter("id", cartFind.getId());
                Cart cart = (Cart) queryCart.getSingleResult();
                cart.setPurchaseDate((new Date()));
                Query query = em.createNamedQuery("Cartrows.findByCartId");
                query.setParameter("cartId", cart);
                List<Cartrows> cartrowList = query.getResultList();
                for (int i = 0; i < cartrowList.size(); i++) {
                    Goods good = cartrowList.get(i).getGoodId();
                    good.setQuantity(good.getQuantity() - cartrowList.get(i).getQuantity());
                }
                EMF.commitTransaction(tx);

                session.removeAttribute("getCart");
            }
        } catch (Exception e) {

        } finally {
            EMF.returnEntityManager(em);
        }
        return checkIfLogged;
    }
}
