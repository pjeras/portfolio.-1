/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;


import com.mycompany.spring.Addresses;
import com.mycompany.spring.Contacts;
import com.mycompany.spring.People;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Lukas
 */
public interface ContactsRep extends JpaRepository<Contacts, Integer> {
    @Query("select c from Contacts c where peopleId = :peopleid")
   List<Contacts> findByPeopleId(@Param("peopleid") People peopleid); 
}
