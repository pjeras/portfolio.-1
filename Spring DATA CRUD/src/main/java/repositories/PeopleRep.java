/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import com.mycompany.spring.People;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Lukas
 */
//People klase o Integer primary key tai id
public interface PeopleRep extends JpaRepository<People, Integer> {
//    ciaquery name patys sukuriam
    @Query("Select p from People p where firstName like :namePart")
    List<People>  findByName(@Param("namePart") String namePart);
//    cia querio name koks yra klaseje people aprasytas
    @Query(name="People.findByLastName")
    List<People>  findByLastName(@Param("lastName") String lastName);
}
