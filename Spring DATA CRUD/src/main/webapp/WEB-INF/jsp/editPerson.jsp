<%-- 
    Document   : editPerson
    Created on : Oct 25, 2018, 2:06:01 PM
    Author     : Lukas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
        <form action="/Spring/savePeople/${person.id}" method="POST">
            <input type="text" placeholder="First name" name="firstName" value="${person.firstName}">
            <input type="text" placeholder="Last name" name="lastName" value="${person.lastName}">
            <input type="text" placeholder="Birth date" name="birthDate" value="${person.birthDate}">
            <input type="number" placeholder="Salary" name="salary" value="${person.salary}">
            <input type="submit">
            
        </form>
        </div>
    </body>
</html>
